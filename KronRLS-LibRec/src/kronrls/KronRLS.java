package kronrls;

import java.io.BufferedReader;
import java.io.FileReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.EigenDecomposition;
import org.apache.commons.math3.linear.RealMatrix;

public class KronRLS {

	private RealMatrix kernelUsuarios;
	private RealMatrix kernelItems;
	private RealMatrix dados;

	public double[][] readRatingData(String diretorioData) {
		ArrayList<ArrayList<Integer>> listaDados = new ArrayList<ArrayList<Integer>>();
		ArrayList<String> listaData = this.readFileData(diretorioData);
		for (String s : listaData) {
			ArrayList<Integer> dados = new ArrayList<Integer>();
			String[] split = s.split("	");

			boolean linhaValida = true;

			for (int j = 0; j < split.length; j++) {
				if (j != 3) {
					int valor;

					try {
						valor = Integer.parseInt(split[j]);
					} catch (Exception e) {
						e.printStackTrace();
						valor = 0;
						linhaValida = false;

					}
					dados.add(valor);
				}
			}

			if (linhaValida) {
				if (dados.get(0) <= this.getKernelUsuarios().getRowDimension()
						&& dados.get(1) <= this.getKernelItems().getRowDimension()) {
					listaDados.add(dados);
				}
			}

		}

		double[][] dadosMatriz = new double[this.getKernelUsuarios().getRowDimension()][this.getKernelItems()
				.getRowDimension()];

		for (ArrayList<Integer> d : listaDados) {
			dadosMatriz[d.get(0) - 1][d.get(1) - 1] = Double.parseDouble(d.get(2).toString());
		}
		
		return dadosMatriz;
	}

	public double[][] readUsersData(String diretorioUser) {
		ArrayList<String> listaUsers = this.readFileData(diretorioUser);
		ArrayList<ArrayList<Integer>> listaUsersI = new ArrayList<ArrayList<Integer>>();
		for (String s : listaUsers) {
			ArrayList<Integer> user = new ArrayList<Integer>();
			String i = s.replace("|", ":");

			i = i.replace("F", "0");
			i = i.replace("M", "1");

			String[] split = i.split(":");

			boolean linhaValida = true;

			for (int j = 0; j < split.length; j++) {
				if (j == 3) {
					String transformarDados = "";
					if (split[j].equals("administrator")) {
						transformarDados = "1|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0";
					} else if (split[j].equals("artist")) {
						transformarDados = "0|1|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0";
					} else if (split[j].equals("doctor")) {
						transformarDados = "0|0|1|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0";
					} else if (split[j].equals("educator")) {
						transformarDados = "0|0|0|1|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0";
					} else if (split[j].equals("engineer")) {
						transformarDados = "0|0|0|0|1|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0";
					} else if (split[j].equals("entertainment")) {
						transformarDados = "0|0|0|0|0|1|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0";
					} else if (split[j].equals("executive")) {
						transformarDados = "0|0|0|0|0|0|1|0|0|0|0|0|0|0|0|0|0|0|0|0|0";
					} else if (split[j].equals("healthcare")) {
						transformarDados = "0|0|0|0|0|0|0|1|0|0|0|0|0|0|0|0|0|0|0|0|0";
					} else if (split[j].equals("homemaker")) {
						transformarDados = "0|0|0|0|0|0|0|0|1|0|0|0|0|0|0|0|0|0|0|0|0";
					} else if (split[j].equals("lawyer")) {
						transformarDados = "0|0|0|0|0|0|0|0|0|1|0|0|0|0|0|0|0|0|0|0|0";
					} else if (split[j].equals("librarian")) {
						transformarDados = "0|0|0|0|0|0|0|0|0|0|1|0|0|0|0|0|0|0|0|0|0";
					} else if (split[j].equals("marketing")) {
						transformarDados = "0|0|0|0|0|0|0|0|0|0|0|1|0|0|0|0|0|0|0|0|0";
					} else if (split[j].equals("none")) {
						transformarDados = "0|0|0|0|0|0|0|0|0|0|0|0|1|0|0|0|0|0|0|0|0";
					} else if (split[j].equals("other")) {
						transformarDados = "0|0|0|0|0|0|0|0|0|0|0|0|0|1|0|0|0|0|0|0|0";
					} else if (split[j].equals("programmer")) {
						transformarDados = "0|0|0|0|0|0|0|0|0|0|0|0|0|0|1|0|0|0|0|0|0";
					} else if (split[j].equals("retired")) {
						transformarDados = "0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|1|0|0|0|0|0";
					} else if (split[j].equals("salesman")) {
						transformarDados = "0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|1|0|0|0|0";
					} else if (split[j].equals("scientist")) {
						transformarDados = "0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|1|0|0|0";
					} else if (split[j].equals("student")) {
						transformarDados = "0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|1|0|0";
					} else if (split[j].equals("technician")) {
						transformarDados = "0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|1|0";
					} else if (split[j].equals("writer")) {
						transformarDados = "0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|1";
					}

					String novoI = transformarDados.replace("|", ":");

					String[] novoSplit = novoI.split(":");

					for (String string : novoSplit) {
						int valor;

						try {
							valor = Integer.parseInt(string);
						} catch (Exception e) {
							e.printStackTrace();
							valor = 0;
							linhaValida = false;

						}
						user.add(valor);
					}
				} else if (j != 0 && j != 4) {
					int valor;

					try {
						valor = Integer.parseInt(split[j]);
					} catch (Exception e) {
						e.printStackTrace();
						valor = 0;
						linhaValida = false;

					}
					user.add(valor);
				}
			}

			if (linhaValida) {
				listaUsersI.add(user);
			}
		}

		int ulinhas = listaUsersI.size();
		int ucolunas = listaUsersI.get(0).size();
		double[][] usersMatriz = new double[ulinhas][ucolunas];

		for (int i = 0; i < ulinhas; i++) {
			for (int j = 0; j < ucolunas; j++) {
				usersMatriz[i][j] = Double.parseDouble(listaUsersI.get(i).get(j).toString());
			}
		}

		return usersMatriz;
	}

	@SuppressWarnings("deprecation")
	public double[][] readItemsData(String diretorioItem) {
		SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");
		ArrayList<String> listaItems = this.readFileData(diretorioItem);
		ArrayList<ArrayList<Integer>> listaItemsI = new ArrayList<ArrayList<Integer>>();
		for (String s : listaItems) {
			ArrayList<Integer> item = new ArrayList<Integer>();
			String i = s.replace("|", "	");

			String[] split = i.split("	");

			boolean linhaValida = true;

			for (int j = 0; j < split.length; j++) {
				int valor;
				if (j == 2) {
					try {
						String d = split[j];
						String[] date = d.split("-");
						Date dataLancamento = new Date(date[1] + "/" + date[0] + "/" + date[2]);

						valor = (int) dataLancamento.getYear();

						item.add(valor);
					} catch (Exception e) {
						linhaValida = false;
					}
				}
				if (j != 0 && j != 3 && j != 1 && j != 2 && j != 4) {
					valor = Integer.parseInt(split[j]);
					item.add(valor);
				}
			}

			if (linhaValida) {
				listaItemsI.add(item);
			}
		}

		int ilinhas = listaItemsI.size();
		int icolunas = listaItemsI.get(0).size();
		double[][] itemsMatriz = new double[ilinhas][icolunas];

		for (int i = 0; i < ilinhas; i++) {
			for (int j = 0; j < icolunas; j++) {
				itemsMatriz[i][j] = Double.parseDouble(listaItemsI.get(i).get(j).toString());
			}
		}
		
		return itemsMatriz;

	}

	public ArrayList<String> readFileData(String diretorioData) {
		ArrayList<String> lista = new ArrayList<String>();
		FileReader data;
		String linha;
		try {
			data = new FileReader(diretorioData);
			BufferedReader lerArquivo = new BufferedReader(data);

			linha = lerArquivo.readLine();
			lista.add(linha);
			while (linha != null) {
				linha = lerArquivo.readLine();
				if (linha != null) {
					lista.add(linha);
				}
			}

			return lista;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public void carregarDados(String diretorioData, String diretorioUser, String diretorioItem) {

		Kernel k;
		
		System.out.println("Carregando dados ---------");

		k = new Kernel(this.readUsersData(diretorioUser));
		k.calculateKernel();

		this.kernelUsuarios = k.getKernel();

		k = new Kernel(this.readItemsData(diretorioItem));
		k.calculateKernel();

		this.kernelItems = k.getKernel();
		this.dados = new Array2DRowRealMatrix(this.readRatingData(diretorioData));

	}

	public RealMatrix getKronRLSRecomenderMatix() {

		RealMatrix A = null;

		if (kernelUsuarios != null && kernelItems != null && dados != null) {
			EigenDecomposition eigenDecompositionUser = new EigenDecomposition(this.getKernelUsuarios());
			EigenDecomposition eigenDecompositionItems = new EigenDecomposition(this.getKernelItems());

			RealMatrix lU = eigenDecompositionUser.getD();
			RealMatrix lI = eigenDecompositionItems.getD();

			RealMatrix qU = eigenDecompositionUser.getV();
			RealMatrix qI = eigenDecompositionItems.getV();
			
			System.out.println("Eign decomposition done --------------");

			int count = 0;
			double[] diagonalLU = new double[lU.getData().length];
			for (double[] d : lU.getData()) {
				double value = d[count];
				diagonalLU[count] = value;
				count++;
			}

			double[][] matrixDiagonalLU = new Array2DRowRealMatrix(diagonalLU).getData();

			count = 0;
			double[] diagonalLI = new double[lI.getData().length];
			for (double[] d : lI.getData()) {
				double value = d[count];
				diagonalLI[count] = value;
				count++;
			}
			
			System.out.println("Diagonais done --------------");

			double[][] matrixDiagonalLIT = new Array2DRowRealMatrix(diagonalLI).transpose().getData();
			
			System.out.println("Start kronecker product of diagonal --------------");
			double[][] l = KroneckerOperation.product(matrixDiagonalLIT, matrixDiagonalLU);

			RealMatrix matrixL = new Array2DRowRealMatrix(l);

			double[][] inverseL = new double[matrixL.getRowDimension()][matrixL.getColumnDimension()];

			System.out.println("Start calculating inverse  --------------");
			
			for (int i = 0; i < inverseL.length; i++) {
				for (int j = 0; j < inverseL[0].length; j++) {
					inverseL[i][j] = 1 / (matrixL.getData()[i][j] + 1.0);
				}
			}

			RealMatrix matrixInverse = new Array2DRowRealMatrix(inverseL);

			RealMatrix m1 = qU.transpose().multiply(this.getDados()).multiply(qI);

			double[][] hadamard = new double[m1.getRowDimension()][m1.getColumnDimension()];
			System.out.println("Start hadamard product  --------------");
			for (int i = 0; i < m1.getRowDimension(); i++) {
				for (int j = 0; j < m1.getColumnDimension(); j++) {
					hadamard[i][j] = m1.getData()[i][j] * matrixInverse.getData()[i][j];
				}
			}

			RealMatrix m2 = new Array2DRowRealMatrix(hadamard);
			System.out.println("Start calculating result  --------------");
			A = qU.multiply(m2).multiply(qI.transpose());
		} else {
			System.out.println("Os dados precisam ser carregados");
		}

		return A;
	}

	public RealMatrix getDados() {
		return dados;
	}

	public void setDados(RealMatrix dados) {
		this.dados = dados;
	}

	public RealMatrix getKernelUsuarios() {
		return kernelUsuarios;
	}

	public void setKernelUsuarios(RealMatrix kernelUsuarios) {
		this.kernelUsuarios = kernelUsuarios;
	}

	public RealMatrix getKernelItems() {
		return kernelItems;
	}

	public void setKernelItems(RealMatrix kernelItems) {
		this.kernelItems = kernelItems;
	}

}
