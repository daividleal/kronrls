package main;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import org.apache.commons.math3.linear.RealMatrix;


import kronrls.KronRLS;

public class Application {
	
	public static void main(String[] args) {
		
		KronRLS kronRLS = new KronRLS();
		
		String dData = "./demo/Datasets/ml-100k/u.data";
		String dUser = "./demo/Datasets/ml-100k/u.user";
		String dItem = "./demo/Datasets/ml-100k/u.item";
		
		System.out.println("u5.test");
		
		kronRLS.carregarDados(dData, dUser, dItem);
		
		System.out.println("Kernel Items dimension: " 
							+ kronRLS.getKernelItems().getColumnDimension() 
							+ " " + kronRLS.getKernelItems().getRowDimension());
		
		System.out.println("Kernel Users dimension: " 
							+ kronRLS.getKernelUsuarios().getColumnDimension() 
							+ " " + kronRLS.getKernelUsuarios().getRowDimension());
		
		System.out.println("Start KronRLS Algorithm --------------");
		
		Long tempoInicio = System.currentTimeMillis();
		
		RealMatrix A = kronRLS.getKronRLSRecomenderMatix();
		
		Long tempoFim = System.currentTimeMillis();
		
		//System.out.println(A.getRowDimension() + " " + A.getColumnDimension());
		/*int linha = 37;
		int coluna = 68;
		System.out.println(A.getData()[linha][coluna] + " " + kronRLS.getDados().getData()[linha][coluna]) ;*/
		
		/*for (int i = 0; i < A.getData().length; i++) {
			for (int j = 0; j < A.getData()[0].length; j++) {
				String tuple = (i+1) + "	" + (j+1)  + "	" + A.getData()[i][j];
				System.out.println(tuple);
			}			
		}*/
		Application.gravarResultados(A.getData());
		
		System.out.println("Tempo total: " + (tempoFim - tempoInicio));
		
		
		/*Demo d = new Demo();
		
		try {
			d.execute(args);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
	}
	
	public static void gravarResultados(double[][] resultado){
		FileWriter arq;
		try {
			arq = new FileWriter("./resultadosKronRLSU5Test.txt");
			PrintWriter gravarArq = new PrintWriter(arq);

		    for (int i = 0; i < resultado.length; i++) {
				for (int j = 0; j < resultado[0].length; j++) {
					gravarArq.printf((i+1) + "	" + (j+1) + "	" + resultado[i][j]+"\n");
				}
			}	      
		    
		    arq.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
	 
	   
	}
}
